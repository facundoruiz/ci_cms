<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Opciones_model extends MY_Model
{

    public $_table = 'opciones';
    public $primary_key = 'key';
    public $order = 'DESC';
    
   
   
    public function dropdown()
    {
      $result =  $this->db->select('key,value')
      ->get($this->_table)
      ->result();
          if(count($result)>0){
  
              foreach ($result as $row)
              {
                $options[$row->id] = $row->name;
              }
              return $options;
          }
      return array();
    }
  
    
  

}
