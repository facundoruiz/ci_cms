<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Post_model extends MY_Model
{
	

    function __construct()
    {
        parent::__construct();

         $this->_table = 'posts';
     $this->primary_key = 'id';
     $this->order = 'DESC';
     $this->soft_delete = TRUE;
     $this->soft_delete_key_full='posts.deleted'; //para realizar filtro soft delete
     $this->before_create = array( 'created_at', 'created_by' );
     $this->before_update = array( 'updated_at','updated_by');
     $this->before_delete = array( 'deleted_at','deleted_by' );
     $this->where = array('deleted' => '0');
   
     $this->user_id_getter='user_id_getter_for_models';
    }

    // datatables
    function json() {
        $this->datatables->select('id,titulo,bajada,contenido,activo,created_at,updated_at,deleted_at,deleted,created_by,updated_by,deleted_by');
        $this->datatables->from('posts');
  
        //add this line for join
        //$this->datatables->join('table2', 'posts.field = table2.field');
        $this->datatables->add_column('action', 
                                        anchor(back_url('post/read/$1'),'Leer',['class'=>'btn btn-info btn-circle btn-sm','title'=>'Ver la imformacion'])." 
                                        ".anchor(back_url('post/update/$1'),'<i class="fas fa-pencil-alt" ></i>',['class'=>'btn btn-warning btn-circle btn-sm','title'=>'Editar'])." 
                                        ".anchor(back_url('post/delete/$1'),'<i class="fas fa-trash"></i>','class="btn btn-danger btn-circle btn-sm" onclick="javasciprt: return confirm(\'Estas seguro ?\')" title="Eliminar"'), 'id');
        return $this->datatables->generate();
    }
   
    protected function callback_after_get($result)
	{
		$this->load->model('Categoria_model', 'categorias');
		$result = parent::callback_after_get($result);

		if ( !empty($result) )
			$result->categorias = $this->categorias->get_by_post_id($result->id);
		
		return $result;
	}

}

/* End of file posts_model.php */
/* Location: ./application/models/posts_model.php */
