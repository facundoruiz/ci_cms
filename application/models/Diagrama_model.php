<?php
//require APPPATH."core/MY_Model.php";

class Diagrama_model extends MY_Model{

  
  
  public $belongs_to = array('articulos' =>
                                  array( 'model' =>'Articulos_model' ,
                                  'primary_key' => 'articulo_id')
                      );

                      function __construct(){
                        parent::__construct();
                        $this->_table = 'diagrama';
                        //$this->primary_key = 'id';
                        $this->order = 'DESC';
                        $this->soft_delete = TRUE;
                        $this->soft_delete_key_full = 'diagrama.deleted';
                        $this->before_create = array( 'created_at', 'created_by' );
                        $this->before_update = array( 'updated_at','updated_by');
                        $this->before_delete = array( 'deleted_at','deleted_by' );
                        
                       
                        $this->user_id_getter='user_id_getter_for_models';
                      }
                    

  }
 ?>
