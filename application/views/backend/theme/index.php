<?php $this->load->view( 'backend/theme/header' );
?>

<body id='page-top'>

  <!-- Page Wrapper -->
  <div id='wrapper'>

    <!-- MENU -->
    <?php $this->load->view( 'backend/theme/menu' );
?>

    <!-- Content Wrapper -->
    <div id='content-wrapper' class='d-flex flex-column'>

      <!-- Main Content -->
      <div id='content'>

        <!-- Begin Page Content -->
        <div class='container-fluid'>

                      <!-- Breadcrumbs -->
                      <?php echo $this->breadcrumbs->show();
                      ?>
                      <!-- notice / messages -->
                      <?php if ( isset( $notice ) ): ?>
                      <?php echo $notice;
                      ?>
                      <?php else: ?>
                      <div id = 'notices'></div>
                      <?php endif;
                      ?>

                      <?=$view_content;
                      ?>
        </div>
        <!-- /.container-fluid-->
    </div>
    <!-- End of Main Content -->

<?php $this->load->view( 'backend/theme/footer' );
?>