 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

   <!-- Sidebar - Brand -->
   <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('admin')?>"
     title="<?=$name_back?>">
     <div class="sidebar-brand-icon rotate-n-15">
       <i class="fas fa-laugh-wink"></i>
     </div>
     <div class="sidebar-brand-text mx-3"><?=$name_back?></div>
   </a>
   <!-- Divider -->
   <hr class="sidebar-divider my-0">
   <?php foreach ($menu as $parent => $parent_params): ?>

   <?php $parent_active =null;
if (empty($parent_params['children'])): ?>
   <!-- Nav Item - <?php echo $parent_params['name']; ?>-->
   <?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
   <li class="nav-item  <?php if ($active) echo 'active'; ?> " title="<?php echo $parent_params['name']; ?>">
     <a class="nav-link" href="<?php echo site_url('admin/'.$parent_params['url']) ; ?>">
       <i class="<?php echo $parent_params['icon']; ?>"></i>
       <span><?php echo $parent_params['name']; ?></span>
     </a>
   </li>
   <?php else: ?>
   <?php if (in_array($ctrler, $parent_params['children']) ) $parent_active ="active"; ?>
   <!-- Nav Item - <?php echo $parent_params['name']; ?> Collapse Menu -->
   <?php //if($parent_active) ?>
   <li class="nav-item">
     <a class="nav-link collapsed" href="#" data-toggle="collapse"
       data-target="#collapse<?php echo $parent_params['name']; ?>" aria-expanded="true"
       aria-controls="collapse<?php echo $parent_params['name']; ?>">
       <i class="<?php echo $parent_params['icon']; ?>"></i>
       <span><?php echo $parent_params['name']; ?></span>
     </a>
     <div id="collapse<?php echo $parent_params['name']; ?>" class="collapse"
       aria-labelledby="heading<?php echo $parent_params['name']; ?>" data-parent="#accordionSidebar">
       <div class="bg-white py-2 collapse-inner rounded">
         <h6 class="collapse-header"><?php echo $parent_params['name']; ?>:</h6>
         <?php foreach ($parent_params['children'] as $name => $url): ?>
         <?php $child_active = ($ctrler==$url); ?>
         <a class="collapse-item" href='<?php echo site_url('admin/'.$url); ?>'><i class="fa fa-circle-o "></i>
           <?php echo $name; ?> </a>
         <?php endforeach; ?>
       </div>
     </div>
   </li>

   <?php endif; ?>

   <?php endforeach; ?>
   <!-- Divider 
      <hr class="sidebar-divider">

     Heading 
      <div class="sidebar-heading">
        Interface
      </div>-->
   <!-- Divider -->
   <hr class="sidebar-divider d-none d-md-block">

   <!-- Sidebar Toggler (Sidebar) -->
   <div class="text-center d-none d-md-inline">
     <button class="rounded-circle border-0" id="sidebarToggle"></button>
   </div>

 </ul>
 <!-- End of Sidebar -->