<!-- Custom css  -->
<link href="<?php echo base_url();?>assets/css/diagrama.css" rel="stylesheet" />
<script src='<?php echo base_url();?>assets/js/jquery-ui-1.12.1/jquery-ui.min.js'></script>


<div class="board-layout">

  <div class="left">
    <div class="board-text">Today Board</div>

  </div>

  <p>
    <h2>jQuery Drag and Drop TODO List with PHP MySQL</h2>
  </p>

  <div class="container-fluid">
    <div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="panel-title">Articulos</h1>
          </div>
          <div id="container1" class="">
            <?php foreach ($articulos as $key => $item) { ?>
            
              <div class="item" data-itemid=<?php echo $item['id'] ?>>
              <?php echo $item['titulo'] ?>
              <p class="itemContent"><?php echo $item['bajada'] ?></p>
            </div>
            
            <?php } ?>

          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="panel-title">Home</h1>
          </div>
          <div id="container2" class="panel-body box-container">
            <?php 

if(@$home['articulos']){
  foreach ($home['articulos'] as $citem) { ?>

            <div class="item" data-itemid=<?php echo $item['id'] ?>>

              <p><strong><?php echo $citem->titulo ?></strong></p>

              <hr />

              <p><?php echo $citem->bajada ?></p>

            </div>

            <?php } 
  }
?>
          </div>
        </div>
      </div>

    </div>
  </div>

 




</div>

  <script>
    $(function () {

      $('.item').draggable({
        cursor: 'move',
        helper: "clone"
      });

      $("#container1").droppable({
        drop: function (event, ui) {
          // var itemid = $(event.originalEvent.toElement).attr("itemid");

          var itemid = ui.draggable.data('itemid');
          //  console.log(itemid)
          $('.item').each(function () {
            // console.log($(this).data("itemid"))
            if ($(this).data("itemid") === itemid) {
              $(this).appendTo("#container1");
            }
          });
        }
      });

      $("#container2").droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function (event, ui) {
          //    var itemid = $(event.originalEvent.toElement).attr("itemid");
          var itemid = ui.draggable.data('itemid');
          //        console.log(itemid)
          $('.item').each(function () {
            
            if ($(this).data("itemid") === itemid) {
             // console.log($(this).data("itemid") + "< - >" + itemid);
              $(this).appendTo("#container2");

            }
          });
          $.ajax({
            method: "POST",
            url: "update_item_status.php",
            data: {
              'itemid': itemid
            },
          }).done(function (data) {
            var result = $.parseJSON(data);

          });
        }
      });

      /*     $('.articulos').droppable({
             accept: '.item',
             hoverClass: 'hovering',
             drop: function (ev, ui) {
               ui.draggable.detach();
               $(this).append(ui.draggable);
               var itemid = ui.draggable.attr('data-itemid')

               $.ajax({
                 method: "POST",

                 url: "update_item_status.php",
                 data: {
                   'itemid': itemid
                 },
               }).done(function (data) {
                 var result = $.parseJSON(data);

               });
             }
           });
           $(function () {
             $("#home").sortable();
             $("#home").disableSelection();
           });*/
    });
  </script>