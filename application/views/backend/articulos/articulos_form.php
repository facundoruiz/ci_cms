
        <h2 style="margin-top:0px">Articulos <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Titulo <?php echo form_error('titulo') ?></label>
            <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Titulo" value="<?php echo $titulo; ?>" />
        </div>
	    <div class="form-group">
            <label for="bajada">Bajada <?php echo form_error('bajada') ?></label>
            <textarea class="form-control" rows="3" name="bajada" id="bajada" placeholder="Bajada"><?php echo $bajada; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="longtext">Contenido <?php echo form_error('contenido') ?></label>
            <textarea class="form-control" rows="3" name="contenido" id="contenido" placeholder="contenido"><?php echo $contenido; ?></textarea>

        </div>
        <div class="form-group">
            <label for="categoria">categoria <?php echo form_error('categoria') ?></label>
            <?php
            foreach ($categoria as $key => $value) {
                echo $value;?>

                <input type="checkbox" class="" name="categoria[]" id="categoria<?php echo $key ?>"  value="<?php echo $key ?>" />
            <?php
            }
            ?>
        </div>
	    <div class="form-group">
            <label for="activo">Activo <?php echo form_error('activo') ?></label>
            
		<div class="form-check">
			<input class="form-check-input" type="radio" name="activo" id="Radios1" value="1" <?php echo $activo==1?'checked':''; ?>>
			<label class="form-check-label" for="Radios1">
				Si
			</label>
		</div>
		<div class="form-check ">
			<input class="form-check-input" type="radio" name="activo" id="Radios2" value="2" <?php echo $activo==2?'checked':''; ?>>
			<label class="form-check-label" for="Radios2">
				No
			</label>
		</div>
	   </div>

	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
	    <a href="<?php echo site_url('admin/articulos') ?>" class="btn btn-default">Cancel</a>
	</form>
     <script src='<?php echo base_url(); ?>assets/js/ckeditor.js'></script>

    <script src='<?php echo base_url(); ?>assets/js/plugins/embed/plugin.js' type="module"></script>

    <script src='<?php echo base_url(); ?>assets/ckfinder/ckfinder.js'></script>


<script>
//import MediaEmbed from '<?php echo base_url(); ?>assets/js/plugins/embed/plugin.js';
	ClassicEditor
		.create( document.querySelector( '#contenido' ), {
               ckfinder: {
               // Upload the images to the server using the CKFinder QuickUpload command.
                uploadUrl: '<?php echo base_url(); ?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json',
            // Define the CKFinder configuration (if necessary).
                options: {
                    resourceType: 'Images'
                }
            },
           // plugins: ['MediaEmbed'],
            toolbar: [ 'ckfinder',
             'source', '-',  'preview',
            , 'heading', '|',
             'bold', 'italic', 'link', 'underline', 'strikethrough', 'code','|',
            'bulletedList', 'numberedList','|',
            'blockQuote','imageUpload','mediaEmbed'],
            
		} )
		.then( editor => {
			window.CKEDITOR = editor;
		} )
		.catch( err => {
			console.error( err.stack );
        } );
        
</script>