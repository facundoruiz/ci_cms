
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Entradas</h2>
            </div>
            
            <div class="col-md-4 text-right">
                <?php echo anchor(back_url('post/create'), 'Nuevo', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        
<div class="card shadow mb-4">
  <div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover " id="mytable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Titulo</th>
		    <th>Bajada</th>
		    <th>Activo</th>
		    <th>Usuario</th>
            <th>Alta</th>
		     <th width="100px">Action</th>
                </tr>
            </thead>
	    
        </table>
        </div>
  </div>
</div>
        <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#mytable").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    language: {
                        "url": JS_BASE_URL +"../assets/vendor/datatables/Spanish.json"
                    },
          
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "post/json", "type": "POST"},
                    columns: [
                        {"data": "id","orderable": false},
                        {"data": "titulo"},
                        {"data": "bajada"},
                        {"data": "activo"},
                        {"data": "created_by"},
                        {"data": "created_at"},
                        {"data" : "action","orderable": false, }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
    