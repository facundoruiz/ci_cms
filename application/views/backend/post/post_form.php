
       
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Titulo <?php echo form_error('titulo') ?></label>
            <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Titulo" value="<?php echo $titulo; ?>" />
        </div>
	    <div class="form-group">
            <label for="bajada">Bajada <?php echo form_error('bajada') ?></label>
            <textarea class="form-control" rows="3" name="bajada" id="bajada" placeholder="Bajada"><?php echo $bajada; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="contenido">Contenido <?php echo form_error('contenido') ?></label>
            <textarea class="form-control" rows="5" name="contenido" id="contenido"><?php echo $contenido; ?></textarea>

        </div>
        <div class="form-group">
            <label for="categoria">Categoria <?php echo form_error('categoria') ?></label>

            <?php   if(isset($post_categoria)){
            foreach ($categoria as $key => $value) {
            ?><div class="form-check">
        <label class="form-check-label" for="categoria<?php echo $key ?>"> <?php  echo $value; ?>
     <input type="checkbox" class="" name="categoria[]" id="categoria<?php echo $key ?>"  value="<?php echo $key ?>" <?php echo array_key_exists($key, $post_categoria)?'checked':''?> />
     </label>  </div>  <?php
                    }                
  }else{
    foreach ($categoria as $key => $value) {
        ?><div class="form-check">
    <label class="form-check-label" for="categoria<?php echo $key ?>"> <?php  echo $value; ?>
 <input type="checkbox" class="" name="categoria[]" id="categoria<?php echo $key ?>"  value="<?php echo $key ?>"  />
 </label>  </div> <?php
                }               
  }
  ?>
           
        </div>
	    <div class="form-group">
            <label for="activo">Activo <?php echo form_error('activo') ?></label>
            
		<div class="form-check">
        <label class="form-check-label" for="Radios1">Si
        	<input class="" type="radio" name="activo" id="Radios1" value="1" <?php echo $activo==1?'checked':''; ?>>
					
			</label>
		</div>
		<div class="form-check ">
        <label class="form-check-label" for="Radios2">	No
        <input class="" type="radio" name="activo" id="Radios2" value="2" <?php echo $activo==2?'checked':''; ?>>
		
			</label>
		</div>
	   </div>

	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
	    <button type="submit" class="btn btn-primary float-right btn-icon-split">
         <span class="text"><?php echo $button ?></span>
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span></button>
	    <a href="<?php echo back_url('post') ?>" class="btn btn-secondary">Cancel</a>
	</form>
     <script src='<?php echo base_url(); ?>assets/js/ckeditor.js'></script>
     <script src='<?php echo base_url(); ?>assets/ckfinder/ckfinder.js'></script>


<script>
ClassicEditor
		.create( document.querySelector( '#contenido' ), {
              ckfinder: {
                   
            // Upload the images to the server using the CKFinder QuickUpload command.
                uploadUrl: '<?php echo base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json',
            // Define the CKFinder configuration (if necessary).
                options: {
                    resourceType: 'Images'
                }
            },
            toolbar: [ 'ckfinder', 'heading', '|', 'bold', 'italic', 'link','imageUpload','mediaEmbed'],
           
		} )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>

<style>
.ck-editor__editable {
    min-height: 250px;
}
</style>         