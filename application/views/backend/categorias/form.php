
<?php echo $form->messages(); ?>


<?php echo $form->open(); ?>
		<div class="panel panel-info ">
      <div class="panel-heading">
        <h3 class="panel-title">Categorias</h3>
      </div>
      <div class="panel-body">

        <?php $combo[0]='- Sin Categoria padre -';
        echo $form->bs3_dropdown('Categoria padre','parent', $combo,$id_parent,array( "placeholder"=>"Agregar Categoria Padre",'autofocus'=>'autofocus')); ?>
  <?php echo $form->field_hidden('id',$campos['id']); ?>
     
        <div class="">
	<?php echo $form->bs3_text('Nombre <span class="text-danger">(*)</span>', 'name',$campos['name'],array('required'=>'required')); ?>
 
                    </div>
                    <p id="status-cat"></p>
 <?php if(!empty($campos['img'])): ?>

<img src="<?php echo site_url(UPLOAD_IMG.$campos['img']); ?>" class="img-rounded" width="200px">

<?php endif;?>

<?php  if($campos['name'])
echo $form->bs3_upload('Imagen','path',null,['class'=>'form-control file','accept'=>'image/*']);?>
<div id="uploaded_image"></div>
            </div>
       
  

<div class="panel-footer">
  <?php echo $form->bs3_submit('Guardar'); ?>

</div>
<?php echo $form->close(); ?>



<script type="text/javascript">


$(document).ready(function() {
  

    var message_status = $("#status-cat");

    $('#form-Categorias').bootstrapValidator({

    }).on('success.form.bv', function(e) {

e.preventDefault();
var formData = new FormData($(this)[0]);
var field =  $(this).serialize();
            $.ajax({
                  url: $(this).attr('action'),
                  type: "post",
                  data: formData,
                  //data:  field,
                  async: false,
                  cache: false,
                  contentType: false,
                  processData: false,
                  }).done(function(d) {
                                          message_status.show();
                                          message_status.html(d.message);
                                          
                                          if(d.success){
                                            setTimeout(function(){message_status.hide()},3000);
                                            bootbox.hideAll(); 
                                           
                                          }

               }).fail(function(jqXHR, textStatus) {
                                          message_status.show();
                                          message_status.text('Error al envio.('+textStatus+') Intente nuevamente desde el principio');
                         });

    });



			/**cortar imagen */
			$image_crop = $('#image_demo').croppie({
				enableExif: true,
				viewport: {
					width: 200,
					height: 200,
					type: 'square' //circle
				},
				boundary: {
					width: 300,
					height: 300
				},
				showZoomer: true,
				//enableResize: true,
				//enableOrientation: true,
			});


			$('#path').on('change', function () {
				var reader = new FileReader();
				reader.onload = function (event) {
					$image_crop.croppie('bind', {
						url: event.target.result
					}).then(function () {
						console.log('jQuery bind complete');
					});
				}
				reader.readAsDataURL(this.files[0]);
				$('#uploadimageModal').modal('show');
			});

			$('.crop_image').click(function (event) {
				$image_crop.croppie('result', {
					type: 'canvas',
					size: 'viewport'
				}).then(function (response) {
					$.ajax({
						url: JS_BASE_URL + "categorias/getImgCrop",
						type: "POST",
						data: {
							"image": response
						},
						success: function (data) {
							$('#uploadimageModal').modal('hide');

							iimg = '<input type= "hidden" name="imagen" id="imagen" value="'+ data +'"/>';


							html = '<img src="'+data+'" class="img-thumbnail"/>';

							$('#uploaded_image').html(iimg + html);
						}
					});
				})
			});



		});

	</script>
</div>


<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Upload & Crop Image</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-8 text-center">
						<div id="image_demo" style="width:350px; margin-top:30px"></div>
					</div>
					<div class="col-md-4" style="padding-top:30px;">
						<br />
						<br />
						<br />
						<button class="btn btn-success crop_image">Aceptar</button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
