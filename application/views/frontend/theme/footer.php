<!-- Footer Section Start -->
<footer id="footer" class="footer-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                <div class="footer-logo mb-3">
                    <img src="<?php echo base_url(UPLOAD_IMG.$opciones['item']->logo)?>" alt="<?=$name_back?>">
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam excepturi quasi, ipsam voluptatem.</p>
          
                <h3 class="footer-titel">Seguime</h3>
                <div class="social-icon">
                    <a class="facebook" href="#"><i class="lni-facebook"></i></a>
                    <a class="twitter" href="#"><i class="lni-twitter"></i></a>
                    <a class="instagram" href="#"><i class="lni-instagram"></i></a>
                    <a class="linkedin" href="#"><i class="lni-linkedin"></i></a>
                    <a class="spotify" href="#"><i class="lni-spotify"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<section id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>Copyright © 2018 Oculux All Right Reserved</p>
            </div>
        </div>
    </div>
</section>

<!-- Go to Top Link -->
<a href="#" class="back-to-top"><i class="lni-arrow-up"></i></a>

<!-- Preloader -->
<div id="preloader">
    <div class="loader">
        <img src="<?php echo base_url(UPLOAD_IMG.$opciones['item']->logo)?>" width="40" height="40" alt="<?=$name_back?>">
        <p>Please wait...</p>
    </div>
</div>

<?php

if (isset($external_scripts['foot']))
{
                   foreach ($external_scripts['foot'] as $script)
                   {
                   $url = strpos($script, 'http') ? $script : base_url('assets/'.$script);
                       echo "<script src='{$url}'></script>\n";
                   }
}
 ?>


</body>
</html>
