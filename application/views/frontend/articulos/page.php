<!-- articulos Section Start -->
<section id="contenido-<?=$item->id?>" class="section-padding">
    <div class="container">
        <div class="section-header text-center wow fadeInDown" data-wow-delay="0.3s">
            <h2 class="section-title"><?=$item->titulo?></h2>
            <p><?=$item->bajada?></p>
        </div>
        <?=$item->contenido?>
    </div>
</section>