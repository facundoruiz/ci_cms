<div id="contact" class="section-padding">
  <div class="container">


    <div class="row">
      <div class="col-12">
        <div class="section-header text-center wow fadeInDown animated" data-wow-delay="0.3s"
          style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
          <h2 class="section-title"> <?php echo lang('login_heading');?> </h2>
        </div>
      </div>
    </div>
    <div class="row contact-form-area wow fadeInUp animated" data-wow-delay="0.4s"
      style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
      <div class="col-md-6 col-lg-4 col-sm-12">
        <div class="contact-right-area wow fadeIn animated" style="visibility: visible;">
          <h2>Get In Touch</h2>
          <div class="contact-right">
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-8 col-sm-12">
        <div class="contact-block">



          <?php echo lang('login_subheading');?>
          <?php echo form_open("usuario/login");?>

          <p>
            <?php echo lang('login_identity_label', 'identity');?>
            <?php echo form_input($identity);?>
          </p>

          <p>
            <?php echo lang('login_password_label', 'password');?>
            <?php echo form_input($password);?>
          </p>

          <p>
            <?php echo lang('login_remember_label', 'remember');?>
            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
          </p>

          <p>
            <?php echo form_submit('submit', lang('login_submit_btn'),array('class' =>'btn btn-primary btn-block' ));?>
          </p>

          <?php echo form_close();?>

          <p><a href="<?php echo site_url('admin/auth/forgot_password')?>"
              class="d-block small"><?php echo lang('login_forgot_password');?></a></p>

        </div>

        <div class="col-md-6"><img src="<?php echo base_url('assets/images/logo.png')?>" alt="Mi Control"></div>
      </div>
    </div>
  </div>