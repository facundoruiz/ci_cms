<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Home extends MY_Controller {

public function index()
{
                $this->render();
}
        
public function articulos($permalink = null){
    $data = array();
    if (!$permalink)
        show_404();

   $data['item'] = $this->db->where("permalink", urldecode($permalink))->get("articulos")->row();
    if (!$data['item'])
        show_404();
    //config('title', $data['item']->title . ' | ' . config('title'));

    $this->view('articulos/page')->render($data);
}
}
        
    /* End of file  Home.php */
        
                            