<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class Post extends Authenticated_Controller
{
    function __construct()
    {
        parent::__construct();
      
        $this->load->model('Post_model');
        $this->breadcrumbs->push('Post','admin/post');
        $this->load->model('Categoria_model');
    	
    }

    public function index()
    {
        $this->view('post/post_list')->render();
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Post_model->json();
    }

    public function read($id) 
    {
        $row = $this->Post_model->get($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'titulo' => $row->titulo,
		'bajada' => $row->bajada,
		'contenido' => $row->contenido,
		'activo' => $row->activo,
	    'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
		'deleted_at' => $row->deleted_at,
		'deleted' => $row->deleted,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
        'deleted_by' => $row->deleted_by,
        'post_categoria' => $row->categorias
	    );
            $this->breadcrumbs->push('Leer','post/');
            $this->view('post/post_read')->render( $data);
        } else {
            $this->session->set_flashdata('message', 'Registro no encontrado');
            redirect( back_url('/post'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' =>  back_url('post/create_action'),
	    'id' => set_value('id'),
	    'titulo' => set_value('titulo'),
	    'bajada' => set_value('bajada'),
	    'contenido' => set_value('contenido'),
        'activo' => set_value('activo'),
        'categoria' => $this->Categoria_model->dropdown()
    );
    
        $this->breadcrumbs->push('Nuevo','post/');
        $this->view('post/post_form')->render( $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'titulo' => $this->input->post('titulo',TRUE),
		'bajada' => $this->input->post('bajada',TRUE),
		'contenido' => $this->input->post('contenido',TRUE),
		'activo' => $this->input->post('activo',TRUE),
	
	    );
        $data = $this->security->xss_clean($data);
    
            $id =$this->Post_model->insert($data);
            //guardar las relaciones
            $cat=$this->input->post('categoria',TRUE);

            if(is_array($cat)){
                  foreach ($cat as $key => $value) {
                             $this->Categoria_model->add_relacion($id,$value);
                      }
            }

            $this->session->set_flashdata('message', 'Post Creado Exitosamente');
            redirect( back_url('post'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Post_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Modificar',
                'action' =>  back_url('post/update_action'),
		'id' => set_value('id', $row->id),
		'titulo' => set_value('titulo', $row->titulo),
		'bajada' => set_value('bajada', $row->bajada),
		'contenido' => set_value('contenido', $row->contenido),
		'activo' => set_value('activo', $row->activo),
        'categoria' => $this->Categoria_model->dropdown(),
        
        );
        if($row->categorias){
            $data['post_categoria']= $row->categorias;
        }
        $this->breadcrumbs->push('Modificar','/');
       
            $this->view('post/post_form')->render( $data);
        } else {
            $this->session->set_flashdata('message', 'Registro no encontrado');
            redirect( back_url('post'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'titulo' => $this->input->post('titulo',TRUE),
		'bajada' => $this->input->post('bajada',TRUE),
		'contenido' => $this->input->post('contenido',TRUE),
		'activo' => $this->input->post('activo',TRUE),
			    );
                
                $id=$this->input->post('id', TRUE);

                $this->Post_model->update( $id, $data);

              //guardar las relaciones
              $cat=$this->input->post('categoria',TRUE);
              $this->Categoria_model->del_relacion($id);#sacamos todos las categorias
              if(is_array($cat)){
                     foreach ($cat as $key => $value) {
                               $this->Categoria_model->add_relacion( $id,$value);
                        }
              }
  
            $this->session->set_flashdata('message', ':) Se Modifico con Exito!');
           redirect( back_url('post'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Post_model->get($id);

        if ($row) {
            $this->Post_model->delete($id);
            $this->session->set_flashdata('message', 'Se borro Exitosamente');
            redirect( back_url('/post'));
        } else {
            $this->session->set_flashdata('message', 'Registro no encontrado');
            redirect( back_url('/post'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('titulo', 'titulo', 'trim|required');
	$this->form_validation->set_rules('bajada', 'bajada', 'trim|required');
	$this->form_validation->set_rules('contenido', 'contenido', 'trim|required');
	$this->form_validation->set_rules('activo', 'activo', 'trim|required');
	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
