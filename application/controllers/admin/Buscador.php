<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Buscador extends MY_Controller {
	public $data;
	public function __construct()
	{
		parent::__construct();
		$this->mTitle = 'Buscador';
		
		$this->load->model('Buscador_model');

	}

public function index() {

	$this->layout('bootbox')->render();
}

public function json() {
	header('Content-Type: application/json');
	echo $this->Buscador_model->json();
}


}
?>
