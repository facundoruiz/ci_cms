<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Clases para diagramar el home
 * 
 */
class Diagrama extends Authenticated_Controller 
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Diagrama_model');
    $this->load->model('Articulos_model');
  }

  public function index()
  {
    $data['articulos']=$this->Articulos_model->as_array()->get_all();
    $data['home']=$this->Diagrama_model->as_array()->with('articulos')->get_all();
    $this->render($data);
  }

}


/* End of file Diagrama.php */
/* Location: ./application/controllers/Admin/Diagrama.php */