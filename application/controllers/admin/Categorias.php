<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categorias extends Authenticated_Controller {
	public $data;
	public function __construct()
	{
		parent::__construct();
		$this->mTitle = 'Categorias de Productos ';
		$this->breadcrumbs->push('Categorias','admin/categorias');
		$this->load->model('Categoria_model');
		

	}
	public function index() {

		$this->render();
	}
	
	public function json() {
		header('Content-Type: application/json');
		echo $this->Categoria_model->json();
	}
	
	public function showForm() {

		$form = $this->form_builder->create_form('admin/categorias/guardar',TRUE,['id'=>'form-Categorias']);
		$this->data['form'] = $form;
		$this->data['combo']=	$this->Categoria_model->dropdown();

			$this->data['campos']=['id'=>set_value('id',null),
			'name'=>set_value('name'),
				];
		$this->data['id_parent']=set_value('parent',0);

		$this->breadcrumbs->push('Nuevo','admin/categorias/');

	$this->view('categorias/form')->layout('bootbox')->render($this->data);
}


public function editForm($p_id) {

	$form = $this->form_builder->create_form("admin/categorias/guardar/$p_id",TRUE,['id'=>'form-Categorias']);
	$this->data['form'] = $form;
	$this->data['combo']=	$this->Categoria_model->dropdown();
	$this->breadcrumbs->push('Modificar','categorias/');
	$this->add_style('vendor/croppie/croppie.css', true);
	$this->add_script('vendor/croppie/croppie.js', true, 'head');
	if(is_numeric($p_id)){

		$campos = $this->Categoria_model->as_array()->get($p_id);

		if(!is_array($campos)){
			$this->set_message('No hay Categorias para Modificar','warning');
			redirect('admin/categorias');
		}

		$this->data['id_parent']=$campos['id_parent'];
		$this->data['campos']=$campos;

	}
$this->view('categorias/form')->render($this->data);
}

protected function _rules($p_id=null)
{
	
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	if(is_null($p_id))
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required|is_unique[categorys.name]', ['is_unique' => '  %s ya existe.']);
	else
		$this->form_validation->set_rules('name', 'Nombre', "trim|callback__edit_unique[categorys.name.$p_id]", ['_edit_unique' => '  %s ya existe.']);
}



function guardar($p_id=null){


	$this->_rules($p_id);

	if ($this->form_validation->run() == true) {

		
	$datos['id']=$this->input->post('id',TRUE);
	$datos['id_parent']=$this->input->post('parent',TRUE);
	$datos['name']=$this->input->post('name');
		
		
		if(is_null($p_id)){

			$this->Categoria_model->insert($datos);
		//	$this->session->set_flashdata('message', 'Cargado con Exito');
			$json['message'] ='Cargado con Exito';
			$json['success']=true;
		}else{
			if(!empty($_FILES['path']['name'])){
				//$picture= subir_img('path',UPLOAD_PRODUCT_IMG);
				//$datos['imagen'] = $picture ;
		
				$img = $_POST["imagen"];
			
				$image_array_1 = explode(";", $img);
		
				$image_array_2 = explode(",", $image_array_1[1]);
			
				$img = base64_decode($image_array_2[1]);
			
				$imageName = date("YmdHisu") . '.png';
			
				file_put_contents(UPLOAD_IMG.$imageName, $img);
			
				$datos['img']=$imageName;
			  }	
			
			$this->Categoria_model->update($datos['id'],$datos);
			//$this->session->set_flashdata('message', 'Modificado con Exito');
			$json['message']='Modificado con Exito';
			$json['success']=true;
			//redirect('/categorias');
		}
		
		
	}else{
		$json['message'] = validation_errors();
		$json['success']=false;
	}	
	
	$this->render_json($json);

}


public function getCategoriaSync()
    {      
            $titulo  =  $this->input->post('dato');
        
            $data = $this->Categoria_model->get_sync("name like '%".$titulo."%' ");
			echo $this->render_json($data,false);

	}
	
public function _edit_unique($str, $cat)
{

   $this->Categoria_model->_edit_unique($str, $cat);

}

public function getImgCrop()
{

//upload.php

	if (isset($_POST["image"])) {
		$data = $_POST["image"];
		$image_array_1 = explode(";", $data);

		$image_array_2 = explode(",", $image_array_1[1]);

		/*$data = base64_decode($image_array_2[1]);

		$imageName = time() . '.png';

		file_put_contents($imageName, $data);
		 */
		$type = pathinfo($data, PATHINFO_EXTENSION);

		echo $image_array_1[0] .';base64,'. $image_array_2[1];

	}

}
}
?>