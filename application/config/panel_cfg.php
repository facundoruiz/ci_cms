<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Panel config
| -------------------------------------------------------------------------
| ARchivo con configuracion inicial del panel backend
*/

$config['panel_cfg'] = array(
	//CArga desde base de dato las Opciones
	'opciones' => true,
	// Site name
	'name' => 'Mi CMS',
	// Default page title
	'page_title' => 'Mi CMS',
	// Default meta data (name => content)
	'meta'	=> array(
		'author'		=> 'Facundo Ruiz',
		'description'	=> 'Mi Content Management System'
	),
	'debug_config'=> FALSE,
	// Default scripts head / foot
	'scripts' => array(
		'head'	=> array(	'vendor/jquery/jquery.min.js',
							'/vendor/bootstrap/js/bootstrap.min.js',
							'/vendor/bootstrap/js/bootstrap.bundle.min.js',
							'vendor/jquery-easing/jquery.easing.min.js',
						'vendor/datatables/jquery.dataTables.min.js',
						'vendor/datatables/dataTables.bootstrap4.min.js',
						'js/dataTables.keyTable.min.js',
						'vendor/bootstrapValidator/bootstrapValidator.min.js',
						'vendor/jquery.media.js',
						'js/select2.full.min.js',
						'js/bootbox.min.js',
						
					),
		'foot' => array(
			'js/sb-admin-2.min.js',
			'js/micontrol.js'

		)			
		
	),

	// Default stylesheets
	'stylesheets' => array(
		'screen' => array(
					 'vendor/fontawesome-free/css/all.min.css',
					 'css/plugins/bootstrapValidator.min.css',
					 'css/sb-admin-2.min.css',
					 'vendor/datatables/dataTables.bootstrap4.min.css'
					 )
	),

	// Menu items
	'menu' => array( // menu principal
				'inicio'=>array(
					'name'=>'Inicio',
					'url'=>'welcome',
					'icon'=>'fas fa-fw fa-circle'),
				'contenido' => array(
						'name'		=> 'Contenido',
						'url'		=> '/',
						'icon' => 'fas fa-fw fa-file',
						'children' => array(
							//  Name => url
								'Entradas'=>'post',
								'Articulos'=>'articulos',
								'Diagrama'=>'diagrama',
									)		
						),
				'administracion' => array(
						'name'		=> 'Administracion',
						'url'		=> '/',
						'icon' => 'fas fa-fw fa-tachometer-alt',
						'children' => array(
							//  Name => url
								'Categorias'=>'categorias',
								'Rubros'=>'rubros',
									)		
						),
				'users' => array(
					'name'		=> 'Sistema',
					'url'		=> 'users',
					'icon' => 'fas fa-fw fa-cog',
					'children' => array(
						'Usuarios' => 'users',
						'Grupos'=>'users/create_group',
						'Opciones'=>'settings',
									)		
								)
		)						
);
