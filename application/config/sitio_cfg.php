<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Panel config
| -------------------------------------------------------------------------
| ARchivo con configuracion inicial del panel backend
*/

$config['sitio_cfg'] = array(
	//CArga desde base de dato las Opciones
	'opciones' => true,
	// Site name
	'name' => 'Mi CMS',
	// Default page title
	'page_title' => 'Mi CMS',
	// Default meta data (name => content)
	'meta'	=> array(
		'author'		=> 'Facundo Ruiz',
		'description'	=> 'Mi Content Management System'
	),
	'debug_config'=> FALSE,
	// Default scripts head / foot
	'scripts' => array(
		'head'	=> array('js/jquery-min.js',
						'vendor/popper/popper.min.js',
						'js/bootstrap.min.js',			
					),
		'foot' => array(
			'js/wow.js',
			'js/scrolling-nav.js',
			'js/owl.carousel.min.js',
			'js/jquery.nav.js',
			'js/jquery.easing.min.js',
			'vendor/bootstrapValidator/bootstrapValidator.min.js'
			,'js/jquery.slicknav.js'
			,'js/particles.min.js'
			,'js/main.js'
			,'js/particlesjs.js'
		)			
		
	),

	// Default stylesheets
	'stylesheets' => array(
		'screen' => array('css/bootstrap4.min.css',
					 'fonts/line-icons.css',
					 'css/animate.css',
					 'css/slicknav.css',
					 'css/owl.carousel.min.css',
					 'css/owl.theme.css',
					  'css/main.css',
					  'css/responsive.css',
					  )
	),

	// Menu items
	'menu' => array( // menu principal
				'inicio'=>array(
					'name'=>'Inicio',
					'url'=>'welcome',
					'icon'=>'fa fa-fw fa-circle'),
				'Media'=>array(
					'name'=>'Media',
					'url'=>'welcome',
					'icon'=>'fa fa-fw fa-circle'),
				'Blog'=>array(
					'name'=>'Blog',
					'url'=>'welcome',
					'icon'=>'fa fa-fw fa-circle'),
				'Contacto'=>array(
					'name'=>'Contacto',
					'url'=>'welcome',
					'icon'=>'fa fa-fw fa-circle'),	
				/*'contenido' => array(
						'name'		=> 'Contenido',
						'url'		=> '/',
						'icon' => 'fa fa-fw fa-file',
						'children' => array(
							//  Name => url
								'Blog'=>'blog',
								'Page'=>'page',
									)		
						),		*/
				)						
);
