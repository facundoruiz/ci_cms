<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ivan Tcholakov <ivantcholakov@gmail.com>, 2014
 * @license The MIT License, http://opensource.org/licenses/MIT
 */
if (!function_exists('user_id_getter_for_models')) {
    // This function is used by 'User' model observes 'created_by' and 'updated_by'.
    // Don't reuse it for other purposes except for model observers!
    function user_id_getter_for_models() {
        return (int) get_instance()->ion_auth->user()->row()->id;
    }
}


?>