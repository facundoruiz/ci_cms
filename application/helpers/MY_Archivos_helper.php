<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('subir_img'))
{
 function subir_img($value=NULL,$ruta)
{

  $CI =& get_instance();

  if(!empty($_FILES[$value]['name'])){
           $config['upload_path'] =$ruta ;
           $config['allowed_types'] = 'jpg|jpeg|png|gif';
           $tmp= explode(".",$_FILES[$value]['name']);
           $extension = strtolower(end($tmp));
           $nombre = date("YmdHisu").".".$extension;
           $config['file_name'] =  $nombre;//$_FILES[$value]['name'];

           //Load upload library and initialize configuration
             $CI->load->library('upload',$config);
           $CI->upload->initialize($config);

           if( $CI->upload->do_upload($value)){
               $uploadData =  $CI->upload->data();
               $picture = $uploadData['file_name'];
           }else{
               $picture = '';
           }
       }else{
           $picture = '';
       }
       return $picture;
}

}
