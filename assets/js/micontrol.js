$(window).keydown(function(event){
               
  switch (event.keyCode) {
    case 113:
     search();
   break;
   case 115:
   turno();
 break;
             } 
});


  function search(){
    bootbox.hideAll();
                 $.get(JS_BASE_URL +"buscador",function(data){
                    $('#modalBuscador').html(data);
                  });

                    bootbox.alert({
                            title:'Consulta de Pacientes',
                            message:  $('#modalBuscador'),
                            backdrop: true,
                            size:'large',
                            }).on('shown.bs.modal', function() {
                                  $('#modalBuscador').show()   ; /* Reset form */
                            }).on('hide.bs.modal', function(e) {
                                $('#modalBuscador').hide().appendTo('body');
                            }).modal('show');
                   
  }
  function turno(){
    /*bootbox.hideAll();
                 $.get(JS_BASE_URL +"calendar/",function(data){
                    $('#modalSale').html(data);
                  });

                    bootbox.dialog({
                            title:'Reserva de Turnos',
                            message:  $('#modalSale'),
                            backdrop: true,
                            size:'large',
                            }).on('shown.bs.modal', function() {
                                  $('#modalSale').show()   ; 
                            }).on('hide.bs.modal', function(e) {
                                $('#modalSale').hide().appendTo('body');
                            }).modal('show');*/
                              window.location=     JS_BASE_URL +"calendar/";
  }

 
  $(document).ready(function () {
      //inicializo el msjbox
      MjsPop.init({
        "selector": "#msj_box"
    });

    $('[data-toggle="tooltip"]').tooltip()
    
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


/**
 *  MENsaje al costato flotante
 * Mjs.show('Msj a mostrar');
 */
var MjsPop = (function() {
    "use strict";

    var elem,
        hideHandler,
        that = {};

    that.init = function(options) {
        elem = $(options.selector);
    };

    that.show = function(text,tipo) {
        clearTimeout(hideHandler);
        if(tipo){
            elem.addClass('alert-'+tipo);
        }else{
            elem.addClass('alert-success');
        }
        
        elem.find("span").html(text);
        elem.delay(200).fadeIn().delay(4000).fadeOut();
    };

    return that;
}());



/**
 * Autocompletado 
 */

function CreateAutoComplete(sourceArray , labelTextBox, idTextBox, _labelQuitar='label-quitar') 
{

            $("#" + labelTextBox).autocomplete({ 
                    source: JS_BASE_URL + sourceArray,
                    minLength: 2,
                    focus: function (event, ui) {
                        event.preventDefault();
                        $("#" + labelTextBox).val(ui.item.label);
                        $("#" + idTextBox).val(ui.item.value);
                        return false;
                    },

                   change: function (_event, ui) {
                        if (ui.item == null) {
                            $("#" + labelTextBox).val("");
                            $("#" + idTextBox).val("");
                        }           
                    },

                    select: function (_event, ui) {
                        $(this).val(ui.item.label);
                        $("#" + labelTextBox).prop('readonly', true);
                        $("#" +_labelQuitar).fadeIn('slow'); 
                        //$("#" + labelTextBox).val(ui.item.label);
                        //$("#" + idTextBox).val(ui.item.value);
                        return false;
                    } //,

                });
                
} // function	

/**
 * 
 * @param {nombre del input} labelTextBox 
 * @param {nombre del label a mostrar} Label 
 */

function AutoCompleteDefaultVal(labelTextBox, Label) {
    
    $("#" + labelTextBox ).autocomplete("search", Label);
   
    var menu = $("#" + labelTextBox ).autocomplete("widget");
    
    $(menu[0].children[0]).click();
   // console.log(menu[0].children[0]);*/
}

function modalVentana(p_url,p_data,p_destino){
    

        $.get(JS_BASE_URL + p_url + p_data,function(data){
          $('#' +p_destino).html(data);
        });
              bootbox.dialog({
                  size: 'large',
                  message: $('#' +p_destino),
                       })
                       .on('shown.bs.modal', function() {
                        $('#' +p_destino)
                          .show()   ; /* Reset form */
                  })
                  .on('hide.bs.modal', function(_e) {
                       $('#' +p_destino).hide().appendTo('body');
                  }).modal('show');
                                  
               
}


/* When the user clicks on the button,
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_js_dropdown_filter
toggle between hiding and showing the dropdown content */
function showDropdown(p_name) {
    //document.getElementById("myInput").value='';
    document.getElementById(p_name).classList.toggle("show");
}

function filterFunction(p_name) {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById(p_name);
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}