$(function(){

    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event

    $('#color').colorpicker(); // Colopicker
    



    // Fullcalendar
    $('#calendar').fullCalendar({
         locale: 'es', 
        header: {
            left: 'prev, next, today',
            center: 'title',
             right: 'month,agendaWeek,agendaDay'
        },

        defaultView: 'agendaWeek',
        // Get all events stored in database
        eventLimit: true, // allow "more" link when too many events
        events: JS_BASE_URL+'calendar/getEvents',
        selectable: true,
         navLinks: true,
        selectHelper: true,
        editable: true, // Make the event resizable true   
        displayEventTime: true,

            select: function(start, end) {
                
                $('#start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                $('#end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                 // Open modal to add event
            modal({
                // Available buttons when adding
                buttons: {
                    add: {
                        id: 'add-event', // Buttons id
                        css: 'btn-success', // Buttons class
                        label: 'Agregar' // Buttons label
                    }
                },
                title: 'Agregar Turno' // Modal title
            });
            }, 
           
         eventDrop: function(event, delta, revertFunc,start,end) {  
            
            start = event.start.format('YYYY-MM-DD HH:mm:ss');
            if(event.end){
                end = event.end.format('YYYY-MM-DD HH:mm:ss');
            }else{
                end = start;
            }         
                       
               $.post(JS_BASE_URL+'calendar/dragUpdateEvent',{                            
                id:event.id,
                start : start,
                end :end
            }, function(result){
                $('.alert').addClass('alert-success').text('Evento Actualizado con exito!');
                hide_notify();


            });



          },
          eventResize: function(event,dayDelta,minuteDelta,revertFunc) { 
                    
                start = event.start.format('YYYY-MM-DD HH:mm:ss');
            if(event.end){
                end = event.end.format('YYYY-MM-DD HH:mm:ss');
            }else{
                end = start;
            }         
                       
               $.post(JS_BASE_URL+'calendar/dragUpdateEvent',{                            
                id:event.id,
                start : start,
                end :end
            }, function(result){
                $('.alert').addClass('alert-success').text('Evento Actualizado con exito!');
                hide_notify();

            });
            },
          
        // Event Mouseover
        eventMouseover: function(calEvent, jsEvent, view){

            var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
            $("body").append(tooltip);

            $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $('.event-tooltip').fadeIn('500');
                $('.event-tooltip').fadeTo('10', 1.9);
            }).mousemove(function(e) {
                $('.event-tooltip').css('top', e.pageY + 10);
                $('.event-tooltip').css('left', e.pageX + 20);
            });
        },
        eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.event-tooltip').remove();
        },
        // Handle Existing Event Click
        eventClick: function(calEvent, jsEvent, view) {
            // Set currentEvent variable according to the event clicked in the calendar
            currentEvent = calEvent;

            // Open modal to edit or delete event
            modal({
                // Available buttons when editing
                buttons: {
                    delete: {
                        id: 'delete-event',
                        css: 'btn-danger',
                        label: 'Borrar'
                    },
                    update: {
                        id: 'update-event',
                        css: 'btn-success',
                        label: 'Actualizar'
                    }
                },
                title: 'Editar Turno "' + calEvent.title + '"',
                event: calEvent
            });
        }

    });

    // Prepares the modal window according to data passed
    function modal(data) {
        // Set modal title
        $('.modal-title').html(data.title);
        // Clear buttons except Cancel
        $('.modal-footer button:not(".btn-default")').remove();
        // Set input values
        $('#title').val(data.event ? data.event.title : '');        
        $('#description').val(data.event ? data.event.description : '');
        $('#color').val(data.event ? data.event.color : '#3a87ad');
        // Create Butttons
        $.each(data.buttons, function(index, button){
            $('.modal-footer').prepend('<button type="button" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
        })
        //Show Modal
        $('#addCalendar').modal('show');
    }

    // Handle Click on Add Button
    $('#addCalendar').on('click', '#add-event',  function(e){
        if(validator(['title', 'description'])) {
            $.post(JS_BASE_URL+'calendar/addEvent', {
                title: $('#title').val(),
                id_paciente: $('#id_paciente').val(),
                description: $('#description').val(),
                color: $('#color').val(),
                start: $('#start').val(),
                end: $('#end').val()
            }, function(result){
                $('.alert').addClass('alert-success').text('Event added successfuly');
                $('#addCalendar').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
                hide_notify();
            });
        }
    });


    // Handle click on Update Button
    $('#addCalendar').on('click', '#update-event',  function(e){
        if(validator(['title', 'description'])) {
            $.post(JS_BASE_URL+'calendar/updateEvent', {
                id: currentEvent.id,
                title: $('#title').val(),
                id_paciente: $('#id_paciente').val(),
                description: $('#description').val(),
                color: $('#color').val()
            }, function(result){
                $('.alert').addClass('alert-success').text('Evento Actualizado con exito!');
                $('#addCalendar').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
                hide_notify();
                
            });
        }
    });



    // Handle Click on Delete Button
    $('#addCalendar').on('click', '#delete-event',  function(e){
        $.get(JS_BASE_URL+'calendar/deleteEvent?id=' + currentEvent.id, function(result){
            $('.alert').addClass('alert-success').text('Event deleted successfully !');
            $('#addCalendar').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
            hide_notify();
        });
    });

    function hide_notify()
    {
        setTimeout(function() {
                    $('.alert').removeClass('alert-success').text('');
                }, 2000);
    }


    // Dead Basic Validation For Inputs
    function validator(elements) {
        var errors = 0;
        $.each(elements, function(index, element){
            if($.trim($('#' + element).val()) == '') errors++;
        });
        if(errors) {
            $('.error').html('Inserte un titulo y descripcion!');
            return false;
        }
        return true;
    }


  /**
   * Autocompletado de paciente
   */ 
    var paciente=false;
    $("#paciente").autocomplete({
                source: JS_BASE_URL + "pacientes/getSync",
                minLength: 2,
                select: function(event, ui) {
                        $(this).val(ui.item.label);
                        paciente=ui.item;
                        return false;
                },
                focus: function(event, ui) {
                        event.preventDefault(); // without this: keyboard movements reset the input to ''
                        $(this).val(ui.item.label);
                        $("#id_paciente").val(ui.item.id);
                      
                }

        }).autocomplete("instance")._renderItem = function(ul, item) {
                var term = this.term.split(' ').join('|');
                var re = new RegExp("(" + term + ")", "gi");
                var t = item.label.replace(re, "<strong>$1</strong>");
                return $("<li></li>").append("<div>" + t + " </div>")
                                     .appendTo(ul);
        };
});