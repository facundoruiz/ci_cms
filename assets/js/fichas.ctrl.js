$(function(){
    var message_status = $("#status");
    $('#form-paciente').bootstrapValidator()
    .on('success.form.bv', function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            var field = $(this).serialize();
            var loaddialog = bootbox.dialog({
                message: '<p class="text-center">Enviando Informacion</p>',
                closeButton: false
            });
           
        
            $.ajax({
                    url: $(this).attr('action'),
                    type: "post",
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,       
            }).done(function(d) {
                    message_status.show();
                    message_status.html(d.message);
                    
                    if (d.success) {
                            //setTimeout(function() { }, 3000);
                             // do something in the background
                             var $active = $('.wizard .nav-tabs li.active');
                             $active.next().removeClass('disabled');
                             nextTab($active);
                             message_status.hide();
                    }

            }).fail(function(jqXHR, textStatus) {
                    message_status.show();
                    message_status.text('Error al envio.(' + textStatus + ') Intente nuevamente desde el principio');

            });
            loaddialog.modal('hide');
    });
/*
$('#dp-paciente').on('click', '#paciente-guardar',  function(e){
   
       $.post(JS_BASE_URL+'calendar/updateEvent', {
           
        }, function(result){
            $('.alert').addClass('alert-success').text('Evento Actualizado con exito!');
                     hide_notify();
            
        });
   
});*/

})