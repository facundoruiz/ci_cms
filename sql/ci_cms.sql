-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para ci_cms
CREATE DATABASE IF NOT EXISTS `ci_cms` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ci_cms`;

-- Volcando estructura para tabla ci_cms.articulos
DROP TABLE IF EXISTS `articulos`;
CREATE TABLE IF NOT EXISTS `articulos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bajada` text COLLATE utf8mb4_unicode_ci,
  `contenido` longtext COLLATE utf8mb4_unicode_ci,
  `activo` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `permalink` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='contenido general del sitio';

-- Volcando datos para la tabla ci_cms.articulos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `articulos` DISABLE KEYS */;
/*!40000 ALTER TABLE `articulos` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.categorys
DROP TABLE IF EXISTS `categorys`;
CREATE TABLE IF NOT EXISTS `categorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='categorias del produc';

-- Volcando datos para la tabla ci_cms.categorys: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categorys` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorys` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.diagrama
DROP TABLE IF EXISTS `diagrama`;
CREATE TABLE IF NOT EXISTS `diagrama` (
  `articulo_id` bigint(20) DEFAULT NULL,
  `orden` tinyint(3) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contenido del homelanding page';

-- Volcando datos para la tabla ci_cms.diagrama: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `diagrama` DISABLE KEYS */;
/*!40000 ALTER TABLE `diagrama` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.events
DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `color` varchar(7) NOT NULL DEFAULT '#3a87ad',
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `allDay` varchar(50) NOT NULL DEFAULT 'true',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.events: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.groups
DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.groups: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `description`) VALUES
	(1, 'admin', 'Administrator'),
	(2, 'members', 'Usuario');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.login_attempts
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.login_attempts: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.menu
DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.menu: 0 rows
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.menu_group
DROP TABLE IF EXISTS `menu_group`;
CREATE TABLE IF NOT EXISTS `menu_group` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.menu_group: 1 rows
/*!40000 ALTER TABLE `menu_group` DISABLE KEYS */;
INSERT INTO `menu_group` (`id`, `title`) VALUES
	(1, 'Main Menu');
/*!40000 ALTER TABLE `menu_group` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.opciones
DROP TABLE IF EXISTS `opciones`;
CREATE TABLE IF NOT EXISTS `opciones` (
  `key` varchar(100) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `required` tinyint(4) NOT NULL DEFAULT '0',
  `autoload` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.opciones: ~38 rows (aproximadamente)
/*!40000 ALTER TABLE `opciones` DISABLE KEYS */;
INSERT INTO `opciones` (`key`, `value`, `default_value`, `required`, `autoload`, `created_at`, `updated_at`, `deleted_at`, `deleted`, `created_by`, `updated_by`, `deleted_by`) VALUES
	('autor', 'Ruiz Facundo', 'Ruiz Facundo', 0, 1, '2019-09-04 17:06:36', NULL, NULL, 0, 0, 0, 0),
	('behance', '', 'www.behance.com', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('contact_form_email', 'facundoruiz@gmail.com', 'facundoruiz@gmail.com', 0, 1, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('copyright', '2020 - ALPC', '2020 - ALPC', 1, 1, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('cuit', '20-000000-1', NULL, 0, NULL, '2019-09-04 17:21:33', NULL, NULL, 0, 0, 0, 0),
	('default_timezone', 'America/Argentina/Tucuman', 'America/Argentina/Tucuman', 0, 1, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('description', '', 'Sitio web sencillo', 1, 1, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('direction', '', 'ltr', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('disqus_user', '', NULL, 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('dribbble', '', 'https://dribbble.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('facebook', '', 'https://www.facebook.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('favicon', '', NULL, 1, 1, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('flickr', '', 'http://flicker.com', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('google_analytics_id', '', NULL, 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('instagram', '', 'https://www.instagram.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('language', 'espanish', 'english', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('logo', '', NULL, 1, 1, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('meta_description', '', 'Descripcion del sitio', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('pagination_limit', '20', '10', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('phone', '400000', '0', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('pinterest', '', 'https://pinterest.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('razon_social', '', 'Algo', 0, NULL, '2019-09-04 17:22:03', NULL, NULL, 0, 0, 0, 0),
	('reddit', '', 'http://reddit.com', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('snapchat', 'ddfdf', NULL, 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('soundcloud', '', 'https://soundcloud.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('support', '', 'Facundo Ruiz', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('support_mail', '', 'facundoruiz@gmail.com', 0, NULL, '2019-09-04 18:47:38', NULL, NULL, 0, 0, 0, 0),
	('support_phone', '', '000000', 0, NULL, '2019-09-04 18:47:53', NULL, NULL, 0, 0, 0, 0),
	('title', '', 'ALPC', 1, 1, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('tumblr', '', 'https://tumblr.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('twitter', '', 'https://twitter.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('twitter_image', '', NULL, 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('vimeo', '', 'https://vimeo.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('vine', '', 'https://vine.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('visitors', '0', '0', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('webmaster_email', '', 'facundoruiz@gmail.com', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('whatsapp', '', 'https://api.whatsapp.com/send?phone=', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0),
	('youtube', '', 'https://www.youtube.com/', 0, NULL, '2019-09-03 19:15:23', NULL, NULL, 0, 0, 0, 0);
/*!40000 ALTER TABLE `opciones` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.posts
DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bajada` text COLLATE utf8mb4_unicode_ci,
  `contenido` longtext COLLATE utf8mb4_unicode_ci,
  `activo` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `permalink` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='contenido general del sitio';

-- Volcando datos para la tabla ci_cms.posts: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.post_categorys
DROP TABLE IF EXISTS `post_categorys`;
CREATE TABLE IF NOT EXISTS `post_categorys` (
  `post_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ci_cms.post_categorys: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `post_categorys` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_categorys` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
	(1, '127.0.0.1', 'admin', '$2y$08$L.ZRKsEN/LNhBFp4OdTRHO7204UeFSJdYffhM8fQqNeN.4S73gg7y', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1598888939, 1, 'Admin', 'istrator', 'ADMIN', '0'),
	(2, '127.0.0.1', 'test', '$2y$08$WgorWK4FNTI8sN9LlxGT/eWpkeUCUPAFIbY54Hy6nwxugd4lebmt2', NULL, 'test@ci_cms.local', NULL, NULL, NULL, NULL, 1583956604, NULL, 1, 'Prueba', 'Test', 'action 2', '2222');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla ci_cms.users_groups
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla ci_cms.users_groups: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
	(1, 1, 1),
	(2, 2, 2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
