# ci_cms

Un CMS simple escalable


*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.


*******
License
*******
MIT License

Copyright (c) 2017 Facundo Ruiz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********
Resources
*********

-  `CodeIgniter 3 - PHP framework <https://codeigniter.com/>`_
-  `User Guide <https://codeigniter.com/docs>`_
-  `Ion Auth - Authentication library <http://benedmunds.com/ion_auth/>`_
-  `Base MODEL - <http://github.com/jamierumbelow/codeigniter-base-model>`_
-  `CodeIgniter Extends Base Model- <https://github.com/jamierumbelow/codeigniter-base-model>`_
-  `MY_Controller - <https://github.com/lonnieezell/my_controller/blob/master/MY_Controller.php>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `jQuery - JavaScript library <https://jquery.com/>`_
-  `Bootstrap - HTML, CSS, and JS framework <http://getbootstrap.com/>`_
-  `BACKEND : SB Admin - Bootstrap admin template <https://startbootstrap.com/template-overviews/sb-admin/>`_
-  `FRONTEND : Oculux landing page - Bootstrap template <https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507>`_
